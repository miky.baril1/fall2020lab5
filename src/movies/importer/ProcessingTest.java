package movies.importer;

import java.io.*;

public class ProcessingTest {

	public static void main(String[] args) throws IOException {
		//LowerCase
		LowercaseProcessor lower = new LowercaseProcessor(null, null, true);
		String source = "C:\\Users\\mikyb\\Desktop\\Fall2020S3\\ProgrammingIII\\Exercises\\Lab5";
		String destination = "C:\\Users\\mikyb\\Desktop\\Fall2020S3\\ProgrammingIII\\Exercises\\Testinput";
		lower = new LowercaseProcessor(source,destination,true );
		lower.execute();
		//RemoveDuplicates
		String sourceR = "C:\\Users\\mikyb\\Desktop\\Fall2020S3\\ProgrammingIII\\Exercises\\Testinput";
		String destinationR = "C:\\Users\\mikyb\\Desktop\\Fall2020S3\\ProgrammingIII\\Exercises\\Removeoutput";
		RemoveDuplicates remove = new RemoveDuplicates(null,null,false);
		remove = new RemoveDuplicates(sourceR,destinationR,false);
		remove.execute();
		
	}


	
}
