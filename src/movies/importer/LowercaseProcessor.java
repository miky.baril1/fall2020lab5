package movies.importer;

import java.util.ArrayList;

public class LowercaseProcessor extends Processor{


	public LowercaseProcessor(String sourceDir, String outputDir, boolean srcContainsHeader) {
		super(sourceDir, outputDir, true);	
		
	}

	public ArrayList<String> process(ArrayList<String> input) {
		
		ArrayList<String> asLower = new ArrayList<String>();
		
		for(int i=0;i<input.size();i++) {
			
			String letterChange = (input.get(i).toLowerCase());
			asLower.add(letterChange);
		}
		
		return asLower;
		
	}

	

}
