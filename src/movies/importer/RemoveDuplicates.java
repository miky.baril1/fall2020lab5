package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor {
	
	public RemoveDuplicates(String sourceDir, String outputDir, boolean srcContainsHeader) {
		super(outputDir, outputDir, false);
	
	}

	public ArrayList<String> process(ArrayList<String> input) {
		
		ArrayList<String> asLower = new ArrayList<String>();
		
		for(int i = 0; i<input.size();i++) {
			String removecopy = input.get(i);
			if(asLower.contains(removecopy)) {
				continue;
			}
			asLower.add(removecopy);
		}
		return asLower;
	}
	
}
